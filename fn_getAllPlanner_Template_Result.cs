//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planner.Model
{
    using System;
    
    public partial class fn_getAllPlanner_Template_Result
    {
        public int TemplateID { get; set; }
        public string PT_Name { get; set; }
        public string Description { get; set; }
        public int TemplateTypeID { get; set; }
        public string PTT_Name { get; set; }
        public bool ActiveFlag { get; set; }
    }
}
