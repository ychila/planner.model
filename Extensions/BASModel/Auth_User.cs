﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public partial class Auth_User
    {
        public string AreaName { get; set; }
        public string RoleName { get; set; }
    }
}
