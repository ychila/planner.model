﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public partial class Auth_UserBusinessUnit
    {
        public int CompanyId { get; set; }
        public string CompanyCodigo { get; set; }
        public string CompanyName { get; set; }
        public string BusinessUnitName { get; set; }
    }
}