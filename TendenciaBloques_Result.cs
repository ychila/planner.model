//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planner.Model
{
    using System;
    
    public partial class TendenciaBloques_Result
    {
        public string Semana { get; set; }
        public string UnidadNegocio { get; set; }
        public string Producto { get; set; }
        public string Plagas { get; set; }
        public int Umbral { get; set; }
        public Nullable<int> CantBloques { get; set; }
        public Nullable<int> BloquesxInv { get; set; }
        public Nullable<int> CantProducto { get; set; }
    }
}
