//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planner.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Auth_UserFarm
    {
        public int UserFarmID { get; set; }
        public int UserID { get; set; }
        public int FarmID { get; set; }
        public int FlagActive { get; set; }
    }
}
