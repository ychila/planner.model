﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public class Planner_SowingProjection
    {
        public int Semana { get; set; }
        public string UnidadNegocio { get; set; }
        public string Bloque { get; set; }
        public string ProductoVegetal { get; set; }
        public string Variedad { get; set; }
        public string Labor { get; set; }
        public string Actividad { get; set; }
        public decimal Unidades { get; set; }
        public decimal Minutos { get; set; }
        public decimal Personas { get; set; }
        public decimal Rendimiento { get; set; }
        public string ModoCalculo { get; set; }
    }
}
