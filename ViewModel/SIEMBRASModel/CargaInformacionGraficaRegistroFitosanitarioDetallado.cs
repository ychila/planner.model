﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public class CargaInformacionGraficaRegistroFitosanitarioDetallado
    {
        public int Semana { get; set; }
        public int Anio { get; set; }
        public int IDInvernaderos_Codigo { get; set; }
        public string IDPlagas_Nombre { get; set; }
        public string ClasesProductos_Nombre { get; set; }
        public decimal PorcIncidencia { get; set; }
        public decimal PorcSeveridad { get; set; }
        public string Color { get; set; }
    }
}
