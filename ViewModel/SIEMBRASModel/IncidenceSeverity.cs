﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public class IncidenceSeverity
    {
        public string Bloque { get; set; }
        public string Semana { get; set; }
        public int Anio { get; set; }
        public decimal PPIncidencia { get; set; }
        public decimal PPSeveridad { get; set; }
        public string Plaga { get; set; }
        public int Monitoreo { get; set; }
        public int PlagaId { get; set; }
        public string PlagaIdM { get; set; }
        public string Color { get; set; }
    }
}