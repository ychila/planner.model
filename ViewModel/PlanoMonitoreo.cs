﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public class PlanoMonitoreo
    {
        public string Cama { get; set; }
        public int IDCama { get; set; }
        public string Producto { get; set; }
        public string Cuadro1 { get; set; }
        public string Cuadro2 { get; set; }
        public string Cuadro3 { get; set; }
        public string Cuadro4 { get; set; }
        public string Cuadro5 { get; set; }
        public string Fsiembra { get; set; }
    }
}
