//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planner.Model
{
    using System;
    
    public partial class visorDinamicoInc_Result
    {
        public string Finca { get; set; }
        public string Bloque { get; set; }
        public Nullable<int> Semana { get; set; }
        public Nullable<int> Anio { get; set; }
        public Nullable<int> IDInvernaderos { get; set; }
        public Nullable<int> TCamas { get; set; }
        public Nullable<int> TBloque { get; set; }
        public string Plaga { get; set; }
        public int id { get; set; }
        public string Producto { get; set; }
        public string Color { get; set; }
        public Nullable<int> CamasAfec { get; set; }
        public Nullable<int> CuadrosAfec { get; set; }
    }
}
